<?php

class mysky
{


  private $curlVerbose = false;
  private $reponseType = 'html';
 
  /**
   * __construct
   *
   * @params boolean $curlVerbose default false
   *
   */
  public function __construct($curlVerbose = false, $reponseType = 'json')
  {
    $this->curlVerbose          = $curlVerbose;
    $this->defaultResponseType  = $reponseType;  
  }
 
  /**
   * getDefaultResponseType
   *
   */
  public function getDefaultResponseType()
  {
    return "/" . $this->defaultResponseType;
  }


  /**
   * callUrl
   *
   * @params string $slug
   */
  public function call($slug, $season='', $episode='')
  {

  if($season != '' && $episode != '')
  {
    $ch = curl_init(sfConfig::get('app_mysky_url','http://mysky.consultingalpha.net/api') . "/" . $slug . "/season/" . $season . "/episode/" . $episode . $this->getDefaultResponseType());
  }
  else
  {
    $ch = curl_init(sfConfig::get('app_mysky_url','http://mysky.consultingalpha.net/api') . "/" . $slug . $this->getDefaultResponseType());
  }



	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1 );
	curl_setopt($ch, CURLOPT_TIMEOUT, 1 );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_VERBOSE, $this->curlVerbose); 
  $result = curl_exec($ch);
  return $result;
  }
}

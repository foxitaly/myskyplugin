<?php if($resp): ?>

<link rel="stylesheet" type="text/css" href="/myskyPlugin/css/style.css" media="all" />
<link rel="stylesheet" type="text/css" href="/myskyPlugin/css/thickbox.css" media="all" />

<a href="/mysky/api/<?php echo $friendlyUrl;?>" class='zoom myskycalltoaction' title="Programmazione su SKY ON DEMAND">Programmazione su SKY ON DEMAND</a>

<script type="text/javascript">
  jQuery(document).ready(function() {
    jQuery("a.zoom").colorbox({
      width:"500", 
      height:"500"
    });
  });
</script>


<?php endif; ?>

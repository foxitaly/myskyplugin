<?php use_helper('Date');?>

<?php 
  $response = json_decode($resp);
  $data = $response[0];
?>

<?php if($data): ?>

<div class='myskyBlock'>

	<div align="center"><img src="/myskyPlugin/images/logo-sky-on-demand.png" title="SKY ON DEMAND" alt="SKY ON DEMAND" /></div>
    
    <div class="separatore"></div>
    
    <h1><?php echo $data->show_title;?></h1>
    <p>L'episodio <?php echo $data->episode?> della stagione <?php echo $data->season?> &egrave;' disponibile su <strong>SKY ON DEMAND</strong><p>
    <p>Programmazione: dal<?php echo format_date($data->start_date);?> al <?php echo format_date($data->end_date);?></p>
    
    <div class="separatore"></div>
    
    <a href="http://www.sky.it/offerta-sky/servizi/sky-on-demand.html" target='_blank' class="buttonSky">NON HAI SKY ON DEMAND? CLICCA QUI</a>
</div>

<?php endif; ?>

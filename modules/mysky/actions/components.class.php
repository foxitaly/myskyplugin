<?php
/*
 * This file is part of the myskyPlugin a foxtv  package.
 * Mauro D'Alatri <m.dalatri@consultingalpha.net>
 *
 * myskyComponents
 *
 * @package    myskyPlugin - foxtv
 * @author     Mauro D'Alatri <m.dalatri@consultingalpha.net>
 */

class myskyComponents extends sfComponents
{

  public function executeCallToAction() 
  {

    $slug     = ($this->friendlyUrl)  ? $this->friendlyUrl : null;
    $season   = ($this->season)       ? $this->season : null;
    $episode  = ($this->episode)      ? $this->episode : null;

    $m = new mysky();
    $resp = $m->call($slug, $season, $episode);
    
    $this->resp = json_decode($resp);

  }

}

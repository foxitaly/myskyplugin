<?php

/**
 * mysky actions.
 *
 * @package    foxtv
 * @subpackage myskyPlugin
 * @author     Mauro D'Alatri <m.dalatri@consultingalpha.net>
 */
class myskyActions extends sfActions
{
  /*
   * execute api
   *
   */
  public function executeApi(sfWEbRequest $request)
  {
    $m = new mysky();
    $this->resp = $m->call($request->getParameter('slug',null)); 

    $this->setLayout(false); 
  }
}
